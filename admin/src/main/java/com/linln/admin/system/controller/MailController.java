package com.linln.admin.system.controller;

import com.linln.admin.system.validator.MailValid;
import com.linln.common.enums.StatusEnum;
import com.linln.common.utils.EntityBeanUtil;
import com.linln.common.utils.ResultVoUtil;
import com.linln.common.utils.StatusUtil;
import com.linln.common.vo.ResultVo;
import com.linln.component.email.MailServiceUtil;
import com.linln.component.email.POP3ReceiveMailTest;
import com.linln.modules.system.domain.Mail;
import com.linln.modules.system.domain.permission.NBAuth;
import com.linln.modules.system.service.MailService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import static com.linln.modules.system.domain.permission.NBAuth.Group.ROUTER;
import static com.linln.modules.system.domain.permission.NBSysResource.ResType.NAV_LINK;
import static com.linln.modules.system.domain.permission.NBSysResource.ResType.OTHER;

import java.util.List;

/**
 * @author 小懒虫
 * @date 2020/04/19
 */
@Controller
@RequestMapping("/system/mail")
public class MailController {

    @Autowired
    private MailService mailService;
    @Autowired
    private MailServiceUtil mailServiceUtil;

    /**
     * 列表页面
     */
    @GetMapping("/index")
    @RequiresPermissions("system:mail:index")
    @NBAuth(value = "system:mail:index", remark = "邮箱系统列表页面", type = OTHER, group = ROUTER)
    public String index(Model model, Mail mail) {

        // 创建匹配器，进行动态查询匹配
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("subject", match -> match.contains())
                .withMatcher("fromAddress", match -> match.contains())
                .withMatcher("receiveAddress", match -> match.contains())
                .withIgnorePaths("size","messageNumber");

        // 获取数据列表
        Example<Mail> example = Example.of(mail, matcher);
        Page<Mail> list = mailService.getPageList(example);

        // 封装数据
        model.addAttribute("list", list.getContent());
        model.addAttribute("page", list);
        return "/system/mail/index";
    }

    /**
     * 跳转到添加页面
     */
    @GetMapping("/add")
    @RequiresPermissions("system:mail:add")
    @NBAuth(value = "system:mail:add", remark = "邮箱系统添加页面", type = NAV_LINK, group = ROUTER)
    public String toAdd() {
        return "/system/mail/add";
    }

    /**
     * 跳转到编辑页面
     */
    @GetMapping("/edit/{id}")
    @RequiresPermissions("system:mail:edit")
    @NBAuth(value = "system:mail:edit", remark = "邮箱系统编辑页面", type = NAV_LINK, group = ROUTER)
    public String toEdit(@PathVariable("id") Mail mail, Model model) {
        model.addAttribute("mail", mail);
        return "/system/mail/add";
    }

    /**
     * 保存添加/修改的数据
     * @param valid 验证对象
     */
    @PostMapping("/save")
    @RequiresPermissions({"system:mail:add", "system:mail:edit"})
    @ResponseBody
    public ResultVo save(@Validated MailValid valid, Mail mail) {
        // 复制保留无需修改的数据
        if (mail.getId() != null) {
            Mail beMail = mailService.getById(mail.getId());
            EntityBeanUtil.copyProperties(beMail, mail);
        }

        // 保存数据
        mailService.save(mail);
        return ResultVoUtil.SAVE_SUCCESS;
    }

    /**
     * 跳转到详细页面
     */
    @GetMapping("/detail/{id}")
    @RequiresPermissions("system:mail:detail")
    @NBAuth(value = "system:mail:detail", remark = "邮箱系统详细页面", type = NAV_LINK, group = ROUTER)
    public String toDetail(@PathVariable("id") Mail mail, Model model) throws  Exception{
        mail.setIsSeen(true);
        mailService.save(mail);
        model.addAttribute("mail", POP3ReceiveMailTest.getMessage(mail));
        return "/system/mail/detail";
    }

    /**
     * 设置一条或者多条数据的状态
     */
    @RequestMapping("/status/{param}")
    @RequiresPermissions("system:mail:status")
    @ResponseBody
    @NBAuth(value = "system:mail:status", remark = "邮箱系统数据状态", type = NAV_LINK, group = ROUTER)
    public ResultVo status(
            @PathVariable("param") String param,
            @RequestParam(value = "ids", required = false) List<Long> ids) {
        // 更新状态
        StatusEnum statusEnum = StatusUtil.getStatusEnum(param);
        if (mailService.updateStatus(statusEnum, ids)) {
            return ResultVoUtil.success(statusEnum.getMessage() + "成功");
        } else {
            return ResultVoUtil.error(statusEnum.getMessage() + "失败，请重新操作");
        }
    }

    /**
     * 设置一条或者多条数据的状态
     */
    @RequestMapping("/getMail")
    @RequiresPermissions("system:mail:status")
    @ResponseBody public ResultVo getMail() {
        mailServiceUtil.getMail();
        return ResultVoUtil.success("成功");
    }


}