package com.linln.admin.system.validator;

import lombok.Data;

import java.io.Serializable;
import javax.validation.constraints.NotEmpty;

/**
 * @author 小懒虫
 * @date 2020/01/01
 */
@Data
public class TagValid implements Serializable {
    @NotEmpty(message = "标签名称不能为空")
    private String name;
}