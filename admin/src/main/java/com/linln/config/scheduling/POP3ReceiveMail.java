package com.linln.config.scheduling;

import com.linln.common.constant.ParamConst;
import com.linln.component.email.MailServiceUtil;
import com.linln.component.email.POP3ReceiveMailTest;
import com.linln.component.thymeleaf.utility.ParamUtil;

import com.linln.modules.system.repository.MailRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



/**
 * 使用POP3协议接收邮件
 */
@Component
@Slf4j
public class POP3ReceiveMail implements ScheduledTaskJob{
    @Autowired
    private MailServiceUtil mailServiceUtil;

    @Override
    public void run()  {
        log.info("》》》》》》开始获取邮件");
        mailServiceUtil.getMail();
        log.info("》》》》》》结束获取邮件");
    }
}
