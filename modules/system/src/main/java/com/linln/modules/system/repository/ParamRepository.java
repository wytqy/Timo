package com.linln.modules.system.repository;

import com.linln.common.constant.StatusConst;
import com.linln.modules.system.domain.Param;
import com.linln.modules.system.repository.BaseRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 小懒虫
 * @date 2020/01/02
 */
public interface ParamRepository extends BaseRepository<Param, Long> {

     Param findFirstByName(String name);

     @Modifying
     @Transactional
     @Query(value = "update or_param set value = ?1  where id =?2",nativeQuery = true)
      Integer updateValue(String value, Long id);
}