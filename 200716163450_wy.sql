/*
MySQL Backup
Source Server Version: 5.7.17
Source Database: test2
Date: 2020/07/16 16:34:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
--  Table structure for `nb_tag`
-- ----------------------------
DROP TABLE IF EXISTS `nb_tag`;
CREATE TABLE `nb_tag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `nb_tag_refer`
-- ----------------------------
DROP TABLE IF EXISTS `nb_tag_refer`;
CREATE TABLE `nb_tag_refer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `refer_id` bigint(20) NOT NULL,
  `show` tinyint(1) NOT NULL,
  `tag_id` bigint(20) NOT NULL,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `or_about`
-- ----------------------------
DROP TABLE IF EXISTS `or_about`;
CREATE TABLE `or_about` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `create_by` bigint(20) DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7owvgl5ufipsjk9ae4br05bup` (`create_by`),
  KEY `FK92bvhpy73b1j0afiotubrengc` (`update_by`),
  CONSTRAINT `FK7owvgl5ufipsjk9ae4br05bup` FOREIGN KEY (`create_by`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FK92bvhpy73b1j0afiotubrengc` FOREIGN KEY (`update_by`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `or_art`
-- ----------------------------
DROP TABLE IF EXISTS `or_art`;
CREATE TABLE `or_art` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `percent` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `create_by` bigint(20) DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKn4qeh6cc9my13s06a4q60v1e6` (`create_by`),
  KEY `FKaqqrb4g4ve3mxoste3rsts9ek` (`update_by`),
  CONSTRAINT `FKaqqrb4g4ve3mxoste3rsts9ek` FOREIGN KEY (`update_by`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FKn4qeh6cc9my13s06a4q60v1e6` FOREIGN KEY (`create_by`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `or_article`
-- ----------------------------
DROP TABLE IF EXISTS `or_article`;
CREATE TABLE `or_article` (
  `id` bigint(20) NOT NULL,
  `appreciable` bit(1) DEFAULT NULL,
  `approve_cnt` int(11) DEFAULT NULL,
  `commented` bit(1) DEFAULT NULL,
  `content` mediumtext NOT NULL,
  `cover` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `md_content` mediumtext,
  `status` tinyint(4) DEFAULT NULL,
  `summary` varchar(300) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `top` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `url_sequence` varchar(255) DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `cate_refer_id` bigint(20) DEFAULT NULL,
  `create_by` bigint(20) DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK322tmcjtjahkx4d9r9p0qudsk` (`cate_refer_id`),
  KEY `FK5utkj0yymr4drmmt277pflox9` (`create_by`),
  KEY `FKkri99jdgv5tcfptexx65a87p9` (`update_by`),
  CONSTRAINT `FK322tmcjtjahkx4d9r9p0qudsk` FOREIGN KEY (`cate_refer_id`) REFERENCES `or_cate` (`id`),
  CONSTRAINT `FK5utkj0yymr4drmmt277pflox9` FOREIGN KEY (`create_by`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FKkri99jdgv5tcfptexx65a87p9` FOREIGN KEY (`update_by`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `or_cate`
-- ----------------------------
DROP TABLE IF EXISTS `or_cate`;
CREATE TABLE `or_cate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cn_name` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `font_icon` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `create_by` bigint(20) DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKpf14yq6f7smx2gpy5krq2a25y` (`create_by`),
  KEY `FKh9bcfjo1vmmhu6xrbdnqv1iwe` (`update_by`),
  CONSTRAINT `FKh9bcfjo1vmmhu6xrbdnqv1iwe` FOREIGN KEY (`update_by`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FKpf14yq6f7smx2gpy5krq2a25y` FOREIGN KEY (`create_by`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `or_comment`
-- ----------------------------
DROP TABLE IF EXISTS `or_comment`;
CREATE TABLE `or_comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `ip_addr` varchar(255) DEFAULT NULL,
  `ip_cn_addr` varchar(255) DEFAULT NULL,
  `level_id` bigint(20) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `update_date` datetime DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `article_id` bigint(20) DEFAULT NULL,
  `create_by` bigint(20) DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKwcps4x824h9fgn7sv76xlxa6` (`article_id`),
  KEY `FKbg9dqy2gpf9evhadu5bfmcko0` (`create_by`),
  KEY `FK7ok0em9y38xhh2yedmmpbulua` (`update_by`),
  CONSTRAINT `FK7ok0em9y38xhh2yedmmpbulua` FOREIGN KEY (`update_by`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FKbg9dqy2gpf9evhadu5bfmcko0` FOREIGN KEY (`create_by`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FKwcps4x824h9fgn7sv76xlxa6` FOREIGN KEY (`article_id`) REFERENCES `or_article` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `or_link`
-- ----------------------------
DROP TABLE IF EXISTS `or_link`;
CREATE TABLE `or_link` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `create_by` bigint(20) DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK9urk7awbbsi1rox80rly3sg4x` (`create_by`),
  KEY `FKtqdm93t6e0vkok7ogr6vjy708` (`update_by`),
  CONSTRAINT `FK9urk7awbbsi1rox80rly3sg4x` FOREIGN KEY (`create_by`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FKtqdm93t6e0vkok7ogr6vjy708` FOREIGN KEY (`update_by`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `or_mail`
-- ----------------------------
DROP TABLE IF EXISTS `or_mail`;
CREATE TABLE `or_mail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` text,
  `create_date` datetime DEFAULT NULL,
  `from_address` varchar(255) DEFAULT NULL,
  `is_container_attachment` bit(1) DEFAULT NULL,
  `is_reply_sign` bit(1) DEFAULT NULL,
  `is_seen` bit(1) DEFAULT NULL,
  `message_number` int(11) NOT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `receive_address` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `sent_date` datetime DEFAULT NULL,
  `size` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `or_note`
-- ----------------------------
DROP TABLE IF EXISTS `or_note`;
CREATE TABLE `or_note` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `create_by` bigint(20) DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKbuyu6ro2gwgkd0ho5ddr73tit` (`create_by`),
  KEY `FKbo2a8pxoh0isguj1qjm1deuym` (`update_by`),
  CONSTRAINT `FKbo2a8pxoh0isguj1qjm1deuym` FOREIGN KEY (`update_by`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FKbuyu6ro2gwgkd0ho5ddr73tit` FOREIGN KEY (`create_by`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `or_param`
-- ----------------------------
DROP TABLE IF EXISTS `or_param`;
CREATE TABLE `or_param` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `data_type` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `value` text,
  `create_by` bigint(20) DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK56ar3u2j131f46ntlyo0bqunb` (`create_by`),
  KEY `FK4x1vfkrolx98y2pia26c38ykw` (`update_by`),
  CONSTRAINT `FK4x1vfkrolx98y2pia26c38ykw` FOREIGN KEY (`update_by`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FK56ar3u2j131f46ntlyo0bqunb` FOREIGN KEY (`create_by`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `or_scheduled_task`
-- ----------------------------
DROP TABLE IF EXISTS `or_scheduled_task`;
CREATE TABLE `or_scheduled_task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `init_start_flag` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `task_cron` varchar(255) DEFAULT NULL,
  `task_desc` varchar(255) DEFAULT NULL,
  `task_key` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `create_by` bigint(20) DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKd4cjj9eq6nuaxfmlxs8ng8xel` (`create_by`),
  KEY `FK7iy10ellvdfnmneqqsqyk9m9` (`update_by`),
  CONSTRAINT `FK7iy10ellvdfnmneqqsqyk9m9` FOREIGN KEY (`update_by`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FKd4cjj9eq6nuaxfmlxs8ng8xel` FOREIGN KEY (`create_by`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `or_uv`
-- ----------------------------
DROP TABLE IF EXISTS `or_uv`;
CREATE TABLE `or_uv` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `browser` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `from_url` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `now_url` varchar(255) DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `system` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `or_words`
-- ----------------------------
DROP TABLE IF EXISTS `or_words`;
CREATE TABLE `or_words` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `create_by` bigint(20) DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKk4qjxgbaaeteaayymfthh8bw9` (`create_by`),
  KEY `FKrv13jamughwh48u89vgok5p0j` (`update_by`),
  CONSTRAINT `FKk4qjxgbaaeteaayymfthh8bw9` FOREIGN KEY (`create_by`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FKrv13jamughwh48u89vgok5p0j` FOREIGN KEY (`update_by`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sys_action_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_action_log`;
CREATE TABLE `sys_action_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `clazz` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `ipaddr` varchar(255) DEFAULT NULL,
  `message` text,
  `method` varchar(255) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `oper_name` varchar(255) DEFAULT NULL,
  `record_id` bigint(20) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `oper_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK32gm4dja0jetx58r9dc2uljiu` (`oper_by`),
  CONSTRAINT `FK32gm4dja0jetx58r9dc2uljiu` FOREIGN KEY (`oper_by`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sys_dept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  `pids` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `create_by` bigint(20) DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKifwd1h4ciusl3nnxrpfpv316u` (`create_by`),
  KEY `FK83g45s1cjqqfpifhulqhv807m` (`update_by`),
  CONSTRAINT `FK83g45s1cjqqfpifhulqhv807m` FOREIGN KEY (`update_by`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FKifwd1h4ciusl3nnxrpfpv316u` FOREIGN KEY (`create_by`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sys_dict`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `value` text,
  `create_by` bigint(20) DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKag4shuprf2tjot9i1mhh37kk6` (`create_by`),
  KEY `FKoyng5jlifhsme0gc1lwiub0lr` (`update_by`),
  CONSTRAINT `FKag4shuprf2tjot9i1mhh37kk6` FOREIGN KEY (`create_by`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FKoyng5jlifhsme0gc1lwiub0lr` FOREIGN KEY (`update_by`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sys_file`
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `md5` varchar(255) DEFAULT NULL,
  `mime` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `sha1` varchar(255) DEFAULT NULL,
  `size` bigint(20) DEFAULT NULL,
  `create_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKbdd4wds55d8pumw4osf6v687s` (`create_by`),
  CONSTRAINT `FKbdd4wds55d8pumw4osf6v687s` FOREIGN KEY (`create_by`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  `pids` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `create_by` bigint(20) DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKoxg2hi96yr9pf2m0stjomr3we` (`create_by`),
  KEY `FKsiko0qcr8ddamvrxf1tk4opgc` (`update_by`),
  CONSTRAINT `FKoxg2hi96yr9pf2m0stjomr3we` FOREIGN KEY (`create_by`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FKsiko0qcr8ddamvrxf1tk4opgc` FOREIGN KEY (`update_by`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sys_resource`
-- ----------------------------
DROP TABLE IF EXISTS `sys_resource`;
CREATE TABLE `sys_resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group` varchar(255) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `permission` varchar(50) NOT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sys_role_resource`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_resource`;
CREATE TABLE `sys_role_resource` (
  `role_id` bigint(20) NOT NULL,
  `resource_id` bigint(20) NOT NULL,
  PRIMARY KEY (`role_id`,`resource_id`),
  KEY `FKkj7e3cva1e2s3nsd0yghpbsnk` (`resource_id`),
  CONSTRAINT `FK7urjh5xeujvp29nihwbs5b9kr` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`),
  CONSTRAINT `FKkj7e3cva1e2s3nsd0yghpbsnk` FOREIGN KEY (`resource_id`) REFERENCES `sys_resource` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `qq_open_id` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `sex` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `dept_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKb3pkx0wbo6o8i8lj0gxr37v1n` (`dept_id`),
  CONSTRAINT `FKb3pkx0wbo6o8i8lj0gxr37v1n` FOREIGN KEY (`dept_id`) REFERENCES `sys_dept` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FKhh52n8vd4ny9ff4x9fb8v65qx` (`role_id`),
  CONSTRAINT `FKb40xxfch70f5qnyfw8yme1n1s` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FKhh52n8vd4ny9ff4x9fb8v65qx` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records 
-- ----------------------------
INSERT INTO `or_param` VALUES ('0','2020-07-16 15:09:13',NULL,'init_status',NULL,'标记用户是否在「小懒虫」App 的初始化设置页面设置过','1','2020-07-16 15:09:13','1','1','1',NULL), ('1','2020-01-09 12:16:03',NULL,'init_status','40','标记用户是否在「小懒虫」App 的初始化设置页面设置过','1','2020-04-13 23:57:28','1','1','1',NULL), ('2','2020-01-09 12:16:05',NULL,'website_title','41','网站标题的文字','1','2020-04-14 21:26:20','小海豚个人博客','1','1',NULL), ('3','2020-01-09 12:16:05',NULL,'footer_words','41','页脚的文字','1','2020-04-14 21:26:33','<p> <strong>2018-2020 nonelonely.com | 网站备案号：<a href=\"http://www.beian.miit.gov.cn\">闽ICP备18026034号-1</a></strong> </p>','1','1',NULL), ('4','2020-01-09 12:16:05',NULL,'index_top_words','0','首页置顶文字','3','2020-01-09 12:16:05','','1','1',NULL), ('5','2020-01-09 12:16:06',NULL,'wechat_pay','0','微信付款码','3','2020-01-09 12:16:06','','1','1',NULL), ('6','2020-01-09 12:16:06',NULL,'alipay','0','支付宝付款码','3','2020-01-09 12:16:06','','1','1',NULL), ('7','2020-01-09 12:16:06',NULL,'info_label','0','信息板内容','3','2020-01-09 12:16:06','','1','1',NULL), ('8','2020-01-09 12:16:07',NULL,'website_logo_words','41','网站logo的文字','1','2020-04-14 23:31:00','小海豚个人博客','1','1',NULL), ('9','2020-01-09 12:16:07',NULL,'website_logo_small_words','41','网站logo的文字旁的小字','1','2020-04-14 23:30:30','小海豚上线了啊','1','1',NULL), ('10','2020-01-09 12:16:07',NULL,'comment_notice','0','评论置顶公告','3','2020-01-09 12:16:07','','1','1',NULL), ('11','2020-01-09 12:16:08',NULL,'project_top_notice','0','项目置顶公告','3','2020-01-09 12:16:08','','1','1',NULL), ('12','2020-01-09 12:16:08',NULL,'message_panel_words','45','留言板的提示信息文字','1','2020-04-14 23:36:54','<span>本页留言暂时关闭，特此通知！</span>','1','1',NULL), ('13','2020-01-09 12:16:08',NULL,'mail_smpt_server_addr','42','SMTP服务器','1','2020-04-14 21:29:34','smtp.nonelonely.com','1','1',NULL), ('14','2020-01-09 12:16:08',NULL,'mail_smpt_server_port','42','SMTP端口号','1','2020-04-14 21:29:48','465','1','1',NULL), ('15','2020-01-09 12:16:09',NULL,'mail_server_account','42','发件人邮箱','1','2020-04-14 21:30:00','admin@nonelonely.com','1','1',NULL), ('16','2020-01-09 12:16:09',NULL,'mail_sender_name','42','发件人邮箱帐号（一般为@前面部分）','1','2020-04-14 21:30:20','admin@nonelonely.com','1','1',NULL), ('17','2020-01-09 12:16:09',NULL,'mail_server_password','42','邮箱登入密码','1','2020-04-14 21:30:09','','1','1',NULL), ('18','2020-01-09 12:16:10',NULL,'app_id','43','qq登录API的app_id','1','2020-04-14 21:34:56','','1','1',NULL), ('19','2020-01-09 12:16:10',NULL,'app_key','43','qq登录API的app_key','1','2020-04-14 21:52:45','','1','1',NULL), ('20','2020-01-09 12:16:10',NULL,'system_name','41','后台系统名称','1','2020-04-14 23:31:26','小海豚','1','1',NULL), ('21','2020-01-09 12:16:10',NULL,'captcha_open','43','验证码是否开启','1','2020-04-14 21:55:59','true','1','1',NULL), ('22','2020-02-12 15:22:50',NULL,'','0','','3','2020-02-12 15:22:50','11','1','1',NULL), ('23','2020-02-12 16:05:39',NULL,'about_us_title','44','关于我的标题','1','2020-04-14 23:29:11','<span>当你觉得生活没意思，做什么都提不起兴趣时，说明你已经许久没有做出过改变了。长时间待在舒适区，人难免会慢慢丧失斗志，动起来，做出改变，出去看看世界或者学一项新技能，让自己始终在进步。你要相信，在这个世界上，一定有着另外一个你自己，在做着你不敢做的事，过着你想过的生活。</span>','1','1',NULL), ('24','2020-02-12 16:14:14',NULL,'lot_title','41','首页热门网站标题','1','2020-04-14 23:31:44','每个人都有一个习惯，我的习惯是在这里等你来临。\r\n<br>','1','1',NULL), ('25','2020-02-12 19:34:17',NULL,'detail_notice','45','博文页面的提示信息','1','2020-04-14 23:37:07','温馨提示：本站所有文章，若非特别声明，均为原创，转载请注明作者及原文链接。','1','1',NULL), ('26','2020-02-13 12:03:36',NULL,'all_comment_open','45','是否全局开放评论','1','2020-04-14 23:37:15','1','1','1',NULL), ('27','2020-02-13 12:04:38',NULL,'comment_keyword','45','评论关键字过滤','1','2020-04-14 23:37:26','尼玛|我擦','1','1',NULL), ('28','2020-02-13 20:10:32',NULL,'website_info','44','网站简介','1','2020-04-19 22:59:59','<p>\r\n	<span>由于一开始之前的博客被人攻击，导致了好多数据没了，后来虽然找回了一些数据，但是回顾一下之前的博客系统很不满意，代码风格很冷，所以后面直接开始重构代码，其实就是重新开发，主要用了</span><span>&nbsp;SpringBoot2.0 + Spring Data Jpa + Thymeleaf + Shiro 开发的后台管理系统，采用分模块的方式便于开发和维护，目前支持的功能有：权限管理、部门管理、字典管理、日志记录、文件上传、代码生成 ，博客模块，系统参数模块等！有了代码生成对于后面开发前端数据很方便，所以我前端模板也重构了</span> \r\n</p>\r\n<h1>\r\n	技术选型\r\n</h1>\r\n<p>\r\n	<br />\r\n</p>\r\n<ol>\r\n	<li>\r\n		后端技术：SpringBoot + Spring Data Jpa + Thymeleaf + Shiro + Jwt + EhCache\r\n	</li>\r\n	<li>\r\n		前端技术：Layui + Font-awesome + nkeditor\r\n	</li>\r\n</ol>\r\n<p>\r\n	<br />\r\n</p>\r\n<h1>\r\n	功能列表\r\n</h1>\r\n<p>\r\n	<br />\r\n</p>\r\n<ul>\r\n	<li>\r\n		用户管理：用于管理后台系统的用户，可进行增删改查等操作。\r\n	</li>\r\n	<li>\r\n		角色管理：分配权限的最小单元，通过角色给用户分配权限。\r\n	</li>\r\n	<li>\r\n		菜单管理：用于配置系统菜单，同时也作为权限资源。\r\n	</li>\r\n	<li>\r\n		部门管理：通过不同的部门来管理和区分用户。\r\n	</li>\r\n	<li>\r\n		字典管理：对一些需要转换的数据进行统一管理，如：男、女等。\r\n	</li>\r\n	<li>\r\n		行为日志：用于记录用户对系统的操作，同时监视系统运行时发生的错误。\r\n	</li>\r\n	<li>\r\n		文件上传：内置了文件上传接口，方便开发者使用文件上传功能。\r\n	</li>\r\n	<li>\r\n		代码生成：可以帮助开发者快速开发项目，减少不必要的重复操作，花更多精力注重业务实现。\r\n	</li>\r\n	<li>\r\n		表单构建：通过拖拽的方式快速构建一个表单模块。\r\n	</li>\r\n	<li>\r\n		数据接口：根据业务代码自动生成相关的api接口文档\r\n	</li>\r\n	<li>\r\n		系统参数管理：设置网站一些值，如网站名称等等\r\n	</li>\r\n	<li>\r\n		博客系统管理：包括博文，笔记，标签，评论，类别管理等等\r\n	</li>\r\n	<li>\r\n		定时任务调度模块：更容易开发的定时任务，可以随时管理定时任务的状态，如时间，启动，关闭，是否开机就启动等。（已完成，等待开源）\r\n	</li>\r\n	<li>\r\n		系统环境监测：使用实时查看系统的cpu,内存状态，可以获取系统的各种配置信息包括网卡，MAC等<span>（已完成，等待开源）</span>\r\n	</li>\r\n	<li>\r\n		后台系统查看实时日志：后台用webSocket实现增加实时查看日志功能，再也不用登录linux系统获取日志文件了<span><span>（已完成，等待开源）</span></span> \r\n	</li>\r\n	<li>\r\n		邮箱功能：<span>（已完成收件功能）</span><br />\r\n	</li>\r\n	<li>\r\n		插件化管理：待开发\r\n	</li>\r\n</ul>\r\n<p>\r\n	<br />\r\n</p>\r\n<h1>\r\n	开源\r\n</h1>\r\n<p>\r\n	&nbsp; &nbsp;取于开源，回报开源，本系统所有代码开源及后续的升级开发代码。若你有什么新的需求或者发现了什么BUG，欢迎反馈！！！&nbsp;开源地址：<a href=\"https://gitee.com/linping0124/lazy_bk\">https://gitee.com/linping0124/lazy_bk</a>&nbsp;&nbsp;\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<h2>\r\n	<strong>QQ群：</strong> \r\n</h2>\r\n<p>\r\n	321225959\r\n</p>','1','1',NULL), ('29','2020-02-13 20:14:14',NULL,'website_info_title','41','网站说明','1','2020-04-14 23:32:49','<p> <span>一个Java程序员的个人博客，记录博主学习和成长之路，分享Java方面技术和源码</span> </p>','1','1',NULL), ('30','2020-02-14 00:05:08',NULL,'frendly_link_info','44','友链页面说明','1','2020-04-14 23:29:34','<p> 本站欢迎交换友链，如需交换链接请在留言板留言，博主不定期对友链进行互动访问。 </p> \r\n<h1> 申请格式 </h1> \r\n<div> \r\n <p> <span>名称：小海豚</span> </p> \r\n <p> <span>网址：</span>https://www.nonelonely.com </p> \r\n <p> <span>图标：</span>https://www.nonelonely.com/favicon.ico </p> \r\n <p> <span>描述：</span>一个Java程序员的个人博客，记录博主学习和成长之路。 </p> \r\n</div> \r\n<h1> 申请要求 </h1> \r\n<div> \r\n <i></i> \r\n <span>原创优先</span> \r\n <i></i> \r\n <span>技术优先</span> \r\n <i></i> \r\n <span>经常宕机</span> \r\n <i></i> \r\n <span>不合法规</span> \r\n <i></i> \r\n <span>插边球站</span> \r\n <i></i> \r\n <span>红标报毒</span> \r\n</div> \r\n<p> 希望您在申请本站友链之前请先做好本站链接，申请提交后若无其它额外因素将于72小时内审核，如超过时间还未通过，请私信我。 </p> \r\n<p> 注意本站会不定期清理违规友链，如果检测到该链接已无法正常访问，本站依然会为您保留30天恢复期，逾期则剔除该链接。 </p> \r\n<p> 如若发现互换友链后未经通知私自撤换友链者，本站将拉黑并永不交换，敬请见谅。 </p> \r\n<h1> The End </h1>','1','1',NULL), ('31','2020-02-14 12:38:30',NULL,'author_info','44','个人说明','1','2020-04-14 23:28:18','<h1> 个人信息 </h1> \r\n<p> 网络昵称：小海豚 </p> \r\n<p> 规格：长≈1.8m，净重≈65KG，误差范围±1.5%，数据仅供参考，一切以实物为准 </p> \r\n<p> 产品成分：水约60%，碳水化合物以及脂肪约14%，蛋白质约17%，其他维生素、矿物质、纤维素约9%。 </p> \r\n<p> 原产地：Made in China </p> \r\n<p> 生产批次：95后 </p> \r\n<p> 保质期：长期有效 </p> \r\n<p> 等级：特级 </p> \r\n<p> 外部包装：原装原配，无拆无修 </p> \r\n<p> 产品介绍：猿界小菜鸡，性别男，爱好女，产品已通过国家质量体系认证，手续齐全，稳定运转二十余年不宕机。 </p> \r\n<p> 功能：我们不生产代码，我们只是代码的搬运工。尽量不生产BUG，非可控因素除外 </p> \r\n<p> 适用人群：老少咸宜 </p> \r\n<p> 注意事项：以上信息，最终解释权归厂家所有。 </p> \r\n<h1> 个人介绍 </h1> \r\n<p> 因为爱好促使我选择了计算机专业，也正因为爱好使得了原本枯燥无味的代码增加了两分趣味。 建立博客的目的是想记录自己学习过程中遇到的问题，自己在闲暇之余喜欢练练字，因为练字能使人静下心来远离喧嚣以及内心的浮躁，把练字当做一种放松的方式也算是一种享受。 个人平时喜欢独处，相比于集体环境，我更倾向于独立生活。每个人来到这个世界都是独自来的，所以孤独是一个人的生命本质，学会独处，实际上那是在去学习生命最本质的东西。 独处会让我们有充分的时间思考、完善、精细自己的一切，规划生活的快慢；独处能使人认清自己有多少能量，哪些可为哪些不可为，切实踏实地面对生活学习，一步一个脚印走好每一步。 而思考总是在孤独中产生的，有时候一个人沉思的孤独，看似很心酸，实则更为强大。没有人生来强大，遥远的梦，遥远的你，与其空怀揣梦想，还不如付之行动。 一个人青春的保质期不长，我不想以后为自己的碌碌无为而悔恨，为自己的平庸无奇而懊恼，只要还有梦就应该勇敢去追寻。 优秀的人很多，只有高度自律，才能使自己变得优秀，希望有一天我也能活出自己所期待的样子。像福尔摩斯一样，穿梭街巷观察平时注意不到的细节， 寻觅潜藏在生活里的魔法钥匙。仰望头顶总能见到那盏高悬的月亮，再黯淡的星光也闪烁着来自银河的诗意在流浪，从生活的边角，到远方的故乡。 后来许多人问我一个人夜晚踟蹰路上的心情，我想起的却不是孤单和路长，而是波澜壮阔的海洋和天空中闪耀的星光... </p> \r\n<h1> The End </h1>','1','1',NULL), ('32','2020-02-14 12:39:02',NULL,'author_name','44','个人名称','1','2020-04-14 23:28:28','小海豚','1','1',NULL), ('33','2020-02-14 12:39:43',NULL,'author_title','44','个人简介','1','2020-04-14 23:28:50','一个朝九晚五的上班族，软件工程专业，善于Java语言。 \r\n<br>','1','1',NULL), ('34','2020-02-14 12:40:53',NULL,'author_addr','44','个人地址','1','2020-04-14 23:29:01','中国-厦门','1','1',NULL), ('35','2020-02-14 12:42:19',NULL,'author_tel','44','个人联系信息','1','2020-04-14 23:32:24','<div> \r\n <a><strong> \r\n   <table> \r\n    <tbody> \r\n     <tr> \r\n      <td> <span>&lt;div <span>class</span>=\"<span>layui-col-md4 layui-col-md-offset4</span>\"&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;a <span>target</span>=\"<span>_blank</span>\" <span>title</span>=\"<span>QQ</span>\" <span>href</span>=\"<a>javascript:void(0);</a>\" </span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>onclick</span>=\"<span>var device = layui.device(); </span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span> if (device.ios || device.android) </span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span> $(this).attr(\'href\', \'mqqwpa://im/chat?chat_type=wpa&amp;uin=\'+ 207915154 +\'&amp;version=1&amp;src_type=web&amp;web_src=oicqzone.com\'); </span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span> else </span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span><span> $(this).attr(\'href\', \'http://wpa.qq.com/msgrd?v=1&amp;uin=\'+ 207915154 +\'&amp;site=qq&amp;menu=yes\');</span>\"&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;i <span>class</span>=\"<span>layui-icon layui-extend-QQ</span>\"&gt;</span><span>&lt;/i&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;/a&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;a <span>href</span>=\"<a>javascript:void(0);</a>\" <span>onclick</span>=\"<span>$.layerMsg(\'微信暂时不对外开放，请更换其他联系方式\');return false;</span>\"&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;i <span>class</span>=\"<span>layui-icon layui-extend-weixin</span>\"&gt;</span><span>&lt;/i&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;/a&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;a <span>target</span>=\"<span>_blank</span>\" <span>href</span>=\"<a>javascript:void(0);</a>\" <span>onclick</span>=\"<span>$.layerMsg(\'微博链接未进行设置\');return false;</span>\"&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;i <span>class</span>=\"<span>layui-icon layui-extend-weibo</span>\"&gt;</span><span>&lt;/i&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;/a&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;a <span>target</span>=\"<span>_blank</span>\" <span>href</span>=\"<a>javascript:void(0);</a>\" <span>onclick</span>=\"<span>$.layerMsg(\'Github链接未进行设置\');return false;</span>\"&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;i <span>class</span>=\"<span>layui-icon layui-extend-GitHub</span>\"&gt;</span><span>&lt;/i&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;/a&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;a <span>href</span>=\"<a href=\"mailto:207915154@qq.com\">mailto:207915154@qq.com</a>\"&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;i <span>class</span>=\"<span>layui-icon layui-extend-mail</span>\"&gt;</span><span>&lt;/i&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;/a&gt;</span> </td> \r\n     </tr> \r\n     <tr> \r\n      <td> </td> \r\n      <td> <span>&lt;/div&gt;</span> </td> \r\n     </tr> \r\n    </tbody> \r\n   </table> </strong> <i></i> </a> \r\n <a> <i></i> </a> \r\n <a> <i></i> </a> \r\n <a> <i></i> </a> \r\n <a href=\"mailto:207915154@qq.com\"> <i></i> </a> \r\n</div>','1','1',NULL), ('36','2020-02-14 13:26:20',NULL,'is_open_message','45','是否开启留言功能','1','2020-04-14 23:37:38','1','1','1',NULL), ('37','2020-02-14 22:15:47',NULL,'key_word','41','网站关键字','1','2020-04-14 23:29:46','springboot,个人博客','1','1',NULL), ('38','2020-03-13 23:50:08',NULL,'visitor_counts','41','网站访问量 计算方法时每天的客户端量加起来。','1','2020-04-23 00:30:00','7519','1','1',NULL), ('40','2020-04-13 23:55:01',NULL,'初始化参数','0','目录','1','2020-04-13 23:55:01','','1','1',NULL), ('41','2020-04-14 21:25:07',NULL,'网站设置','0','目录','1','2020-04-14 21:25:07','','1','1',NULL), ('42','2020-04-14 21:29:22',NULL,'邮箱配置','0','目录','1','2020-04-14 21:29:22','','1','1',NULL), ('43','2020-04-14 21:33:41',NULL,'登录设置','0','目录','1','2020-04-14 21:33:41','','1','1',NULL), ('44','2020-04-14 23:27:42',NULL,'关于配置','0','目录','1','2020-04-14 23:27:42','','1','1',NULL), ('45','2020-04-14 23:36:35',NULL,'评论/留言设置','0','目录','1','2020-04-14 23:36:35','','1','1',NULL);
INSERT INTO `or_scheduled_task` VALUES ('1','2020-04-16 22:49:49','1','1','0 30 0 * * ?','计算访客量','countUv','2020-04-16 23:30:25','1','1'), ('2','2020-04-17 00:25:09','1','1','0 38 0 * * ?','删除前3天的Uv','deleteUv','2020-04-19 00:18:16','1','1'), ('3','2020-04-19 18:21:28','1','1','0 0/5 * * * ? ','定时从邮箱获取数据','POP3ReceiveMail','2020-04-20 20:15:58','1','1');
INSERT INTO `or_uv` VALUES ('1','Chrome 8(83.0.4103.61)','2020-07-16 15:50:37','http://localhost:443/article/pages','0:0:0:0:0:0:0:1','中国-广西-桂林','http://localhost:443/','7537851d-a755-47ba-8f31-d874afc81bcf','1','Win7','2020-07-16 15:54:41','67420069E0E9D7419EAB454A548D13A6'), ('2','Chrome 8(83.0.4103.61)','2020-07-16 15:54:41','http://localhost:443/article/pages','0:0:0:0:0:0:0:1','中国-广西-桂林','http://localhost:443/#welcome','7537851d-a755-47ba-8f31-d874afc81bcf','1','Win7','2020-07-16 15:54:46','67420069E0E9D7419EAB454A548D13A6'), ('3','Chrome 8(83.0.4103.61)','2020-07-16 15:54:46','http://localhost:443/','0:0:0:0:0:0:0:1','中国-广西-桂林','http://localhost:443/article/pages','7537851d-a755-47ba-8f31-d874afc81bcf','1','Win7','2020-07-16 15:55:03','67420069E0E9D7419EAB454A548D13A6'), ('4','Chrome 8(83.0.4103.61)','2020-07-16 15:55:03','http://localhost:443/article/pages','0:0:0:0:0:0:0:1','中国-广西-桂林','http://localhost:443/article/pages','7537851d-a755-47ba-8f31-d874afc81bcf','1','Win7','2020-07-16 15:56:25','67420069E0E9D7419EAB454A548D13A6'), ('5','Chrome 8(83.0.4103.61)','2020-07-16 15:56:25','http://localhost:443/article/pages','0:0:0:0:0:0:0:1','中国-广西-桂林','http://localhost:443/','7537851d-a755-47ba-8f31-d874afc81bcf','1','Win7','2020-07-16 16:04:55','67420069E0E9D7419EAB454A548D13A6'), ('6','Chrome 8(83.0.4103.61)','2020-07-16 16:04:55','http://localhost:443/','0:0:0:0:0:0:0:1','中国-广西-桂林','http://localhost:443/article/pages','7537851d-a755-47ba-8f31-d874afc81bcf','1','Win7','2020-07-16 16:23:09','67420069E0E9D7419EAB454A548D13A6'), ('7','Chrome 8(83.0.4103.61)','2020-07-16 16:23:10','http://localhost:443/article/pages','0:0:0:0:0:0:0:1','中国-广西-桂林','http://localhost:443/','7537851d-a755-47ba-8f31-d874afc81bcf','1','Win7','2020-07-16 16:24:17','67420069E0E9D7419EAB454A548D13A6'), ('8','Chrome 8(83.0.4103.61)','2020-07-16 16:24:18','http://localhost:443/article/pages','0:0:0:0:0:0:0:1','中国-广西-桂林','http://localhost:443/','7537851d-a755-47ba-8f31-d874afc81bcf','1','Win7','2020-07-16 16:24:28','67420069E0E9D7419EAB454A548D13A6'), ('9','Chrome 8(83.0.4103.61)','2020-07-16 16:24:28','http://localhost:443/article/pages','0:0:0:0:0:0:0:1','中国-广西-桂林','http://localhost:443/','7537851d-a755-47ba-8f31-d874afc81bcf','1','Win7',NULL,'67420069E0E9D7419EAB454A548D13A6');
INSERT INTO `or_words` VALUES ('1','2020-02-15 20:39:37','','1','爱过的心总会有梦，流过的泪总有痕迹。','2020-02-23 20:39:54','1','1'), ('2','2020-02-15 20:40:20','','1','希望源于失望，奋起始于忧患。','2020-02-23 20:39:45','1','1'), ('3','2020-02-15 20:41:27','','1','生活是蜿蜒在山中的小径，坎坷不平，沟崖在侧。','2020-02-23 20:39:37','1','1'), ('4','2020-02-15 20:42:37','','1','梦虽虚幻，却是自己的梦想。','2020-02-23 20:39:28','1','1'), ('5','2020-02-21 15:50:54','','1','很多我们以为一辈子都不会忘记的事情,就在我们念念不忘的日子里,被我们遗忘了。','2020-02-23 20:39:21','1','1'), ('6','2020-02-23 20:39:09','','1','穿越千万时间线，只想见你。','2020-02-23 20:39:09','1','1'), ('7','2020-03-03 14:04:18','','1','夏蝉冬雪，不过轮回一瞥。','2020-03-03 14:04:18','1','1'), ('8','2020-03-04 16:02:18','','1','你可以不用成为我生命中的阳光 但你一定是我的归宿。','2020-03-04 16:02:18','1','1'), ('9','2020-03-31 17:32:46','','1','世界上最远的距离 不是瞬间便无处寻觅 而是尚未相遇 便注定无法相聚','2020-03-31 17:32:46','1','1'), ('10','2020-04-13 23:02:18','','1','我没有很想你，只是经常梦见你。','2020-04-13 23:02:18','1','1'), ('11','2020-07-16 16:24:06','','1','你是怎样，世界就是怎样，相信自己','2020-07-16 16:24:06','1','1');
INSERT INTO `sys_action_log` VALUES ('1','com.linln.component.actionLog.exception.ActionLogProceedAdvice','2020-07-16 15:50:36','0:0:0:0:0:0:0:1','java.lang.NullPointerException\n	com.linln.frontend.controller.AboutController.putUvEnd(AboutController.java:222)\n	sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n	sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n	sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n	java.lang.reflect.Method.invoke(Method.java:497)\n	org.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:189)\n	org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:138)\n	org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:102)\n	org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:892)\n	org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:797)\n	org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\n	org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:1038)\n	org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:942)\n	org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:1005)\n	org.springframework.web.servlet.FrameworkServlet.doPost(FrameworkServlet.java:908)\n	javax.servlet.http.HttpServlet.service(HttpServlet.java:660)\n	org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:882)\n	javax.servlet.http.HttpServlet.service(HttpServlet.java:741)\n	org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:231)\n	org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n	org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:53)\n	org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n	org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n	org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\n	org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\n	org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\n	org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\n	org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\n	org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:449)\n	org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:365)\n	org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\n	org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\n	org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\n	org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:362)\n	org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\n	org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n	org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n	com.linln.common.xss.XssFilter.doFilter(XssFilter.java:47)\n	org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n	org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n	org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:99)\n	org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n	org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n	org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n	org.springframework.web.filter.FormContentFilter.doFilterInternal(FormContentFilter.java:92)\n	org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n	org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n	org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n	org.springframework.web.filter.HiddenHttpMethodFilter.doFilterInternal(HiddenHttpMethodFilter.java:93)\n	org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n	org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n	org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n	org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:200)\n	org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\n	org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\n	org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\n	org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:200)\n	org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:96)\n	org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:490)\n	org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:139)\n	org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:92)\n	org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:74)\n	org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:343)\n	org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:408)\n	org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:66)\n	org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:834)\n	org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1415)\n	org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\n	java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)\n	java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)\n	org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)\n	java.lang.Thread.run(Thread.java:745)','run',NULL,'系统异常','系统',NULL,'3',NULL), ('2','com.linln.admin.system.controller.LoginController','2020-07-16 15:56:47','0:0:0:0:0:0:0:1','后台登录成功','login',NULL,'用户登录','管理员',NULL,'2','1');
INSERT INTO `sys_menu` VALUES ('1','2020-01-09 12:16:13','layui-icon layui-icon-home','0','[0]',NULL,'1','1','主页','1','2020-01-09 12:16:13','/system/main/index','1','1'), ('2','2020-01-09 12:16:13','fa fa-cog','0','[0]',NULL,'2','1','系统管理','1','2020-01-09 12:16:13','#','1','1'), ('3','2020-01-09 12:16:13','','2','[0][2]',NULL,'3','1','菜单管理','2','2020-01-09 12:16:13','/system/menu/index','1','1'), ('4','2020-01-09 12:16:13','','2','[0][2]',NULL,'2','1','角色管理','2','2020-01-09 12:16:13','/system/role/index','1','1'), ('5','2020-01-09 12:16:14','','2','[0][2]',NULL,'1','1','用户管理','2','2020-01-09 12:16:14','/system/user/index','1','1'), ('6','2020-02-12 15:21:54','','2','[0],[2]','','4','1','系统参数管理','2','2020-02-12 15:21:54','/system/param/index','1','1'), ('7','2020-02-12 16:35:45','fa fa-files-o','0','[0]','','3','1','博客管理','1','2020-02-12 16:35:45','#','1','1'), ('8','2020-02-12 16:36:24','','7','[0],[7]','','1','1','博文管理','2','2020-02-12 16:36:24','/system/article/index','1','1'), ('9','2020-02-12 16:38:15','','7','[0],[7]','','2','1','分类管理','2','2020-02-12 16:38:15','/system/cate/index','1','1'), ('10','2020-02-13 20:29:36','fa fa-th','0','[0]','','4','1','开发管理','1','2020-02-13 20:29:36','#','1','1'), ('11','2020-02-13 20:30:02','','10','[0],[10]','','1','1','代码生成','2','2020-02-13 20:30:02','/dev/code','1','1'), ('12','2020-02-13 20:35:10','','7','[0],[7]','','3','1','关于管理','2','2020-02-13 20:35:10','/system/about/index','1','1'), ('13','2020-02-14 12:26:50','','7','[0],[7]','','4','1','友链管理','2','2020-02-14 12:26:50','/system/link/index','1','1'), ('14','2020-02-14 12:48:50','','7','[0],[7]','','5','1','技能管理','2','2020-02-14 12:48:50','/system/art/index','1','1'), ('15','2020-02-14 12:55:59','','7','[0],[7]','','6','1','评论/留言管理','2','2020-02-14 12:55:59','/system/comment/index','1','1'), ('16','2020-02-15 20:39:01','','7','[0],[7]','','7','1','美句管理','2','2020-02-15 20:39:01','/system/words/index','1','1'), ('17','2020-03-12 23:21:07','','7','[0],[7]','','8','1','流量统计','2','2020-03-12 23:21:07','/system/uv/index','1','1'), ('18','2020-04-16 22:48:18','','2','[0],[2]','','5','1','定时任务调度','2','2020-04-16 22:48:18','/system/scheduledTask/index','1','1'), ('19','2020-04-16 23:15:49','','2','[0],[2]','','6','1','字典管理','2','2020-04-16 23:15:49','/system/dict/index','1','1'), ('20','2020-04-19 18:08:07','','2','[0],[2]','','7','1','邮箱管理','2','2020-04-19 18:08:07','/system/mail/index','1','1'), ('21','2020-04-21 15:46:04','','2','[0],[2]','','8','3','111','2','2020-04-21 15:52:21','/dev/build','1','1'), ('22','2020-04-22 12:36:54','','2','[0],[2]','','8','1','部门管理','2','2020-04-22 12:36:54','/system/dept/index','1','1');
INSERT INTO `sys_resource` VALUES ('1','ROUTER','关于列表页面','system:about:index',NULL,'OTHER','/system/about/index'), ('2','ROUTER','关于数据状态','system:about:status',NULL,'NAV_LINK','/system/about/status/{param}'), ('3','ROUTER','关于添加页面','system:about:add',NULL,'NAV_LINK','/system/about/add'), ('4','ROUTER','关于编辑页面','system:about:edit',NULL,'NAV_LINK','/system/about/edit/{id}'), ('5','ROUTER','关于详细页面','system:about:detail',NULL,'NAV_LINK','/system/about/detail/{id}'), ('6','ROUTER','日志列表页面','system:actionLog:index',NULL,'OTHER','/system/actionLog/index'), ('7','ROUTER','删除指定日志','system:actionLog:delete',NULL,'NAV_LINK','/system/actionLog/status/delete'), ('8','ROUTER','日志详细页面','system:actionLog:detail',NULL,'NAV_LINK','/system/actionLog/detail/{id}'), ('9','ROUTER','技能列表页面','system:art:index',NULL,'OTHER','/system/art/index'), ('10','ROUTER','技能数据状态','system:art:status',NULL,'NAV_LINK','/system/art/status/{param}'), ('11','ROUTER','技能添加页面','system:art:add',NULL,'NAV_LINK','/system/art/add'), ('12','ROUTER','技能编辑页面','system:art:edit',NULL,'NAV_LINK','/system/art/edit/{id}'), ('13','ROUTER','技能详细页面','system:art:detail',NULL,'NAV_LINK','/system/art/detail/{id}'), ('14','ROUTER','博文列表页面','system:article:index',NULL,'OTHER','/system/article/index'), ('15','ROUTER','博文数据状态','system:article:status',NULL,'NAV_LINK','/system/article/status/{param}'), ('16','ROUTER','博文添加页面','system:article:add',NULL,'NAV_LINK','/system/article/add'), ('17','ROUTER','博文编辑页面','system:article:edit',NULL,'NAV_LINK','/system/article/edit/{id}'), ('18','ROUTER','博文详细页面','system:article:detail',NULL,'NAV_LINK','/system/article/detail/{id}'), ('19','ROUTER','类别列表页面','system:cate:index',NULL,'OTHER','/system/cate/index'), ('20','ROUTER','类别数据状态','system:cate:status',NULL,'NAV_LINK','/system/cate/status/{param}'), ('21','ROUTER','类别添加页面','system:cate:add',NULL,'NAV_LINK','/system/cate/add'), ('22','ROUTER','类别编辑页面','system:cate:edit',NULL,'NAV_LINK','/system/cate/edit/{id}'), ('23','ROUTER','类别详细页面','system:cate:detail',NULL,'NAV_LINK','/system/cate/detail/{id}'), ('24','ROUTER','评论列表页面','system:comment:detail',NULL,'OTHER','/system/comment/index'), ('25','ROUTER','评论数据状态','system:comment:status',NULL,'NAV_LINK','/system/comment/status/{param}'), ('26','ROUTER','评论详细页面','system:comment:detail',NULL,'NAV_LINK','/system/comment/detail/{id}'), ('27','ROUTER','部门列表页面','system:dept:index',NULL,'OTHER','/system/dept/index'), ('28','ROUTER','部门数据状态','system:dept:status',NULL,'NAV_LINK','/system/dept/status/{param}'), ('29','ROUTER','部门添加页面','system:dept:add',NULL,'NAV_LINK','/system/dept/add'), ('30','ROUTER','部门添加页面','system:dept:add',NULL,'NAV_LINK','/system/dept/add/{pid}'), ('31','ROUTER','部门编辑页面','system:dept:edit',NULL,'NAV_LINK','/system/dept/edit/{id}'), ('32','ROUTER','部门详细页面','system:dept:detail',NULL,'NAV_LINK','/system/dept/detail/{id}'), ('33','ROUTER','字典列表页面','system:dict:add',NULL,'OTHER','/system/dict/index'), ('34','ROUTER','字典状态','system:dict:status',NULL,'NAV_LINK','/system/dict/status/{param}'), ('35','ROUTER','字典添加页面','system:dict:add',NULL,'NAV_LINK','/system/dict/add'), ('36','ROUTER','字典编辑页面','system:dict:edit',NULL,'NAV_LINK','/system/dict/edit/{id}'), ('37','ROUTER','字典详细页面','system:dict:detail',NULL,'NAV_LINK','/system/dict/detail/{id}'), ('38','ROUTER','友链列表页面','system:link:index',NULL,'OTHER','/system/link/index'), ('39','ROUTER','友链数据状态','system:link:status',NULL,'NAV_LINK','/system/link/status/{param}'), ('40','ROUTER','友链添加页面','system:link:add',NULL,'NAV_LINK','/system/link/add'), ('41','ROUTER','友链编辑页面','system:link:edit',NULL,'NAV_LINK','/system/link/edit/{id}'), ('42','ROUTER','友链详细页面','system:link:detail',NULL,'NAV_LINK','/system/link/detail/{id}'), ('43','ROUTER','邮箱系统列表页面','system:mail:index',NULL,'OTHER','/system/mail/index'), ('44','ROUTER','邮箱系统数据状态','system:mail:status',NULL,'NAV_LINK','/system/mail/status/{param}'), ('45','ROUTER','邮箱系统添加页面','system:mail:add',NULL,'NAV_LINK','/system/mail/add'), ('46','ROUTER','邮箱系统编辑页面','system:mail:edit',NULL,'NAV_LINK','/system/mail/edit/{id}'), ('47','ROUTER','邮箱系统详细页面','system:mail:detail',NULL,'NAV_LINK','/system/mail/detail/{id}'), ('48','ROUTER','后台主体内容','system:main:index',NULL,'OTHER','/system/main/index'), ('49','ROUTER','菜单列表页面','system:menu:index',NULL,'OTHER','/system/menu/index'), ('50','ROUTER','菜单数据状态','system:menu:detail',NULL,'NAV_LINK','/system/menu/status/{param}'), ('51','ROUTER','菜单添加页面','system:menu:index',NULL,'NAV_LINK','/system/menu/add'), ('52','ROUTER','菜单添加页面','system:menu:index',NULL,'NAV_LINK','/system/menu/add/{pid}'), ('53','ROUTER','菜单编辑页面','system:menu:edit',NULL,'NAV_LINK','/system/menu/edit/{id}'), ('54','ROUTER','菜单详细页面','system:menu:detail',NULL,'NAV_LINK','/system/menu/detail/{id}'), ('55','ROUTER','笔记列表页面','system:note:index',NULL,'OTHER','/system/note/index'), ('56','ROUTER','笔记数据状态','system:note:detail',NULL,'NAV_LINK','/system/note/status/{param}'), ('57','ROUTER','笔记添加页面','system:note:add',NULL,'NAV_LINK','/system/note/add'), ('58','ROUTER','笔记编辑页面','system:note:edit',NULL,'NAV_LINK','/system/note/edit/{id}'), ('59','ROUTER','笔记详细页面','system:note:detail',NULL,'NAV_LINK','/system/note/detail/{id}'), ('60','ROUTER','系统参数列表页面','system:param:index',NULL,'OTHER','/system/param/index'), ('61','ROUTER','系统参数状态','system:param:status',NULL,'NAV_LINK','/system/param/status/{param}'), ('62','ROUTER','系统参数添加页面','system:param:add',NULL,'NAV_LINK','/system/param/add'), ('63','ROUTER','系统参数编辑页面','system:param:edit',NULL,'NAV_LINK','/system/param/edit/{id}'), ('64','ROUTER','系统参数详细页面','system:param:detail',NULL,'NAV_LINK','/system/param/detail/{id}'), ('65','ROUTER','角色列表页面','system:role:index',NULL,'OTHER','/system/role/index'), ('66','ROUTER','修改角色状态','system:role:status',NULL,'NAV_LINK','/system/role/status/{param}'), ('67','ROUTER','角色添加页面','system:role:add',NULL,'NAV_LINK','/system/role/add'), ('68','ROUTER','角色编辑页面','system:role:edit',NULL,'NAV_LINK','/system/role/edit/{id}'), ('69','ROUTER','角色详细页面','system:role:detail',NULL,'NAV_LINK','/system/role/detail/{id}'), ('70','ROUTER','角色授权页面','system:role:auth',NULL,'NAV_LINK','/system/role/auth'), ('71','ROUTER','任务调度列表页面','system:scheduledTask:index',NULL,'OTHER','/system/scheduledTask/index'), ('72','ROUTER','任务调度数据状态','system:scheduledTask:status',NULL,'NAV_LINK','/system/scheduledTask/status/{param}'), ('73','ROUTER','任务调度添加页面','system:scheduledTask:add',NULL,'NAV_LINK','/system/scheduledTask/add'), ('74','ROUTER','任务调度编辑页面','system:scheduledTask:edit',NULL,'NAV_LINK','/system/scheduledTask/edit/{id}'), ('75','ROUTER','任务调度详细页面','system:scheduledTask:detail',NULL,'NAV_LINK','/system/scheduledTask/detail/{id}'), ('76','ROUTER','标签列表页面','system:tag:index',NULL,'OTHER','/system/tag/index'), ('77','ROUTER','标签数据状态','system:tag:status',NULL,'NAV_LINK','/system/tag/status/{param}'), ('78','ROUTER','标签添加页面','system:tag:add',NULL,'NAV_LINK','/system/tag/add'), ('79','ROUTER','标签编辑页面','system:tag:edit',NULL,'NAV_LINK','/system/tag/edit/{id}'), ('80','ROUTER','标签详细页面','system:tag:detail',NULL,'NAV_LINK','/system/tag/detail/{id}'), ('81','ROUTER','用户列表页面','system:user:index',NULL,'OTHER','/system/user/index'), ('82','ROUTER','用户添加页面','system:user:add',NULL,'NAV_LINK','/system/user/add'), ('83','ROUTER','用户编辑页面','system:user:edit',NULL,'NAV_LINK','/system/user/edit/{id}'), ('84','ROUTER','用户详细页面','system:user:detail',NULL,'NAV_LINK','/system/user/detail/{id}'), ('85','ROUTER','用户数据状态','system:user:status',NULL,'NAV_LINK','/system/user/status/{param}'), ('86','ROUTER','用户修改密码页面','system:user:pwd',NULL,'NAV_LINK','/system/user/pwd'), ('87','ROUTER','用户角色分配页面','system:user:role',NULL,'NAV_LINK','/system/user/role'), ('88','ROUTER','统计流量列表页面','system:uv:index',NULL,'OTHER','/system/uv/index'), ('89','ROUTER','统计流量数据状态','system:uv:status',NULL,'NAV_LINK','/system/uv/status/{param}'), ('90','ROUTER','统计流量详细页面','system:uv:detail',NULL,'NAV_LINK','/system/uv/detail/{id}'), ('91','ROUTER','美句列表页面','system:words:index',NULL,'OTHER','/system/words/index'), ('92','ROUTER','美句数据状态','system:words:status',NULL,'NAV_LINK','/system/words/status/{param}'), ('93','ROUTER','美句添加页面','system:words:add',NULL,'NAV_LINK','/system/words/add'), ('94','ROUTER','美句编辑页面','system:words:edit',NULL,'NAV_LINK','/system/words/edit/{id}'), ('95','ROUTER','美句详细页面','system:words:detail',NULL,'NAV_LINK','/system/words/detail/{id}'), ('96','ROUTER','表单生成页面','dev:build:index',NULL,'OTHER','/dev/build'), ('97','ROUTER','代码生成页面','dev:code:index',NULL,'OTHER','/dev/code'), ('98','ROUTER','文档生成页面','dev:build:index',NULL,'OTHER','/dev/swagger');
INSERT INTO `sys_role` VALUES ('1','2020-07-16 15:09:09','admin',NULL,'1','网站管理员','2020-07-16 15:09:12'), ('2','2020-07-16 15:09:12','ROLE_USER',NULL,'1','网站访客','2020-07-16 15:09:12');
INSERT INTO `sys_role_resource` VALUES ('1','1'), ('1','2'), ('1','3'), ('1','4'), ('1','5'), ('1','6'), ('1','7'), ('1','8'), ('1','9'), ('1','10'), ('1','11'), ('1','12'), ('1','13'), ('1','14'), ('1','15'), ('1','16'), ('1','17'), ('1','18'), ('1','19'), ('1','20'), ('1','21'), ('1','22'), ('1','23'), ('1','24'), ('1','25'), ('1','26'), ('1','27'), ('1','28'), ('1','29'), ('1','30'), ('1','31'), ('1','32'), ('1','33'), ('1','34'), ('1','35'), ('1','36'), ('1','37'), ('1','38'), ('1','39'), ('1','40'), ('1','41'), ('1','42'), ('1','43'), ('1','44'), ('1','45'), ('1','46'), ('1','47'), ('1','48'), ('1','49'), ('1','50'), ('1','51'), ('1','52'), ('1','53'), ('1','54'), ('1','55'), ('1','56'), ('1','57'), ('1','58'), ('1','59'), ('1','60'), ('1','61'), ('1','62'), ('1','63'), ('1','64'), ('1','65'), ('1','66'), ('1','67'), ('1','68'), ('1','69'), ('1','70'), ('1','71'), ('1','72'), ('1','73'), ('1','74'), ('1','75'), ('1','76'), ('1','77'), ('1','78'), ('1','79'), ('1','80'), ('1','81'), ('1','82'), ('1','83'), ('1','84'), ('1','85'), ('1','86'), ('1','87'), ('1','88'), ('1','89'), ('1','90'), ('1','91'), ('1','92'), ('1','93'), ('1','94'), ('1','95'), ('1','96'), ('1','97'), ('1','98');
INSERT INTO `sys_user` VALUES ('1','2020-07-16 15:09:09',NULL,'管理员','b1dcaa6fdd6b69d0cce3c7053499f9c852c2f4b3d24061e65ee7d1fc6b51db4a',NULL,NULL,NULL,NULL,'T51D8N',NULL,'1','2020-07-16 15:09:09','admin',NULL);
INSERT INTO `sys_user_role` VALUES ('1','1');
